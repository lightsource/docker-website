#!/bin/bash

# arguments:
# 1. target user name
# 2. docker build command (-t X)

########

userName=$1
command=$2

userId=$(id -u "$userName")
userGroupId=$(id -g "$userName")

docker build ./website $command \
--build-arg userName="$userName" \
--build-arg userId="$userId" \
--build-arg userGroupId="$userGroupId"
