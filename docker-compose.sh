#!/bin/bash

# arguments:
# 1. target user name
# args below required only for 'up'
# [2. target image]
# [3. docker compose command (up -d, down)]

########

command=$1
userName=$2
image=$3

if [[ -n "$userName" ]]; then
  userId=$(id -u "$userName")
  userGroupId=$(id -g "$userName")
fi

export _USER=$userName
export _UID=$userId
export _GID=$userGroupId
export _IMAGE=$image

docker-compose $command
