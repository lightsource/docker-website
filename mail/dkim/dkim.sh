#!/bin/bash

domain=$1
selector=$2

opendkim-genkey -b 2048 -h rsa-sha256 -r -v --subdomains -s "$selector" -d "$domain"
# Fixes https://github.com/linode/docs/pull/620
sed -i 's/h=rsa-sha256/h=sha256/' mail.txt
mv mail.private "$domain".private
mv mail.txt "$domain".txt
