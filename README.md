## What is it

It's an example of a docker image and docker-compose config for a php website (with apache, php-fpm, mysql, phpMyAdmin,
postfix)   
The image should be build for each target user, it'll allow have the right owner for bound folders and a container (
otherwise bound folders will have a root owner, and the target user won't be able to edit it)

## Requirements

1. [docker nginx proxy](https://gitlab.com/lightsource/docker-nginx-proxy)  
   It waits nginx proxy usage (will allow to a docker host have several website containers)
2. `opendkim` and `opendkim-tools` apt packages (for generating a dkim key)

## How to use

**Brackets below (`{}`) in the instruction meaning your specific values.**

Clone the repository, cd to the folder and do steps below:

**1. Run the nginx-proxy**

Only once for all, see [docker nginx proxy](https://gitlab.com/lightsource/docker-nginx-proxy)

**2. Build the image for a specific user**

`bash build.sh "{targetUserName}" "-t {targetImageTag} --no-cache"`

**3. Create the .env file:**

`cp .env-sample .env` then insert your specific values

**4. Create those folders for the target user (do it from the target user)**

`mkdir -p ~/docker/db;`

`mkdir -p ~/docker/phpmyadmin;`

`mkdir -p ~/docker/mail;`

`mkdir -p ~/docker/apache-log;`

`mkdir -p ~/docker/html/website; mkdir -p ~/docker/html/php-tmp;`

`mkdir -p ~/docker/db-backups;`

**5. phpMyAdmin settings (do it from the target user)**

`cp phpmyadmin/* ~/docker/phpmyadmin;`

`htpasswd ~/docker/phpmyadmin/.htpasswd {userName};`

**6. mail settings (do it from the target user)**

`cp mail/* ~/docker/mail -r`

`bash ~/docker/mail/dkim/dkim.sh {domain} {dkimSelector}` - it'll create files with dkim private and public keys in the
same folder

then update the `mailhub={COMPOSE_PROJECT_NAME}_mail:587` line in the ~/docker/mail/ssmtp.conf file.

After it, don't forget to create properly SPF, DKIM, DMARC dns records for the domain.

**7. Run the containers**

`bash docker-compose.sh "{command}" "{targetUser}" "{targetWebsiteImageTag}"`  
Where `{command}` can be any docker-compose command, like `up -d` or `down`.  
For the `down` command only the first arg is required.

**After**

The db is available for the website with a host `{COMPOSE_PROJECT_NAME}_db:3306`, db name is `website`.

The phpMyAdmin is available by a domain, which was set in .env `_PMA_VIRTUAL_HOST`.

The website is available by a domain, which was set in .env `VIRTUAL_HOST`.

## Known issues

**1. SSL**  
The reverse proxy provides SSL, but the website hosted without SSL, so for some CMS/frameworks additional settings can be required.
* [WordPress settings](https://wordpress.org/support/article/administration-over-ssl/#using-a-reverse-proxy)
